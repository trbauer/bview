module Bits where

import Data.Bits
import Data.Word
import qualified Data.ByteString.Lazy as BS

--------------------------------------------------------------------------------
fromByteStringU8 :: BS.ByteString -> Word8
fromByteStringU8 = BS.head
fromByteStringU16LE :: BS.ByteString -> Word16
fromByteStringU16LE = fromByteStringLE
fromByteStringU16BE :: BS.ByteString -> Word16
fromByteStringU16BE = fromByteStringBE
fromByteStringU32LE :: BS.ByteString -> Word32
fromByteStringU32LE = fromByteStringLE
fromByteStringU32BE :: BS.ByteString -> Word32
fromByteStringU32BE = fromByteStringBE
fromByteStringU64LE :: BS.ByteString -> Word64
fromByteStringU64LE = fromByteStringLE
fromByteStringU64BE :: BS.ByteString -> Word64
fromByteStringU64BE = fromByteStringBE

fromByteStringLE :: (FiniteBits i,Integral i) => BS.ByteString -> i
fromByteStringLE = fromByteStringG reverse
fromByteStringBE :: (FiniteBits i,Integral i) => BS.ByteString -> i
fromByteStringBE = fromByteStringG id

fromByteStringG :: (FiniteBits i,Integral i) => ([Word8] -> [Word8]) -> BS.ByteString -> i
fromByteStringG reoder_bytes =
    go num_bytes zero . reoder_bytes . BS.unpack . BS.take (fromIntegral num_bytes)
  where zero = 0
        --
        num_bytes = finiteBitSize zero `div` 8
        --
        -- go :: Int -> i -> [Word8] -> Word64
        go 0   i    _ = i
        go _   _    [] = error "fromByteStringG: insufficient bytes"
        go n   i    (b:bs) = go (n-1) ((i`shiftL`8) .|. fromIntegral b) bs

toWords :: FiniteBits a => (BS.ByteString -> a) -> BS.ByteString -> [a]
toWords func bs0
  | BS.length bs0 `mod` fromIntegral bytes_per_elem /= 0 =
      error $ "Bits.toWords: binary size doesn't divide bit size"
  | otherwise = go bs0
  where bytes_per_elem = finiteBitSize (func undefined)`div`8

        go bs
          | BS.null bs = []
          | otherwise =
            case BS.splitAt (fromIntegral bytes_per_elem) bs of
              (pfx,sfx) -> func pfx:go sfx
