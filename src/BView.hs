module BView where

import Bits
import Floats
import Format

import Control.Monad
import Data.Bits
import Data.Char
import Data.Int
import Data.List
import Data.Word
import Debug.Trace
import System.Exit
import System.IO
import Text.Printf
-- import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BS


-- TODO:
--   * big-endian (flip the lines around BS.reverse)
--   * override floating point format (%e and hex float)
--   * make better README.md file
--   * seek
--   * float dissecting

-- TODO: (from HDV)
--   * sub byte types
--   * special pixel types
--   * vector types
--   * float types
--   * exotic float types
--   * int128
--   * diffing --diff (takes 2 files) iterates in order
--   * print multiple format types (-t=u32,f32) prints unsigned with floats below
--                                  -t=u64,f32x2
--   * struct types -t=struct{f32 foo;bar i32x2;baz f32}
--     or -t=struct{f32;i32x2;f32}
--
--   * find element -f={0xBADBEEF0}, find sequence -f=0xBADBEEF0,0xBADBEEF1
--   * find likely strings (strings.exe) u8, u16


vERSION :: String
vERSION = "1.0.1"

usage :: String
usage =
  "Binary Viewer " ++ vERSION ++ "\n" ++
  "  usage: bv [OPTS] INPUTS+\n" ++
  "where OPTS are:\n" ++
  "      --color={on,off,auto}   colors output\n" ++
  "   -c/--cols=INT              override columns per row\n" ++
  "   -d/--diff                  diff the files (instead of listing them serially)\n" ++
  "                              specify the reference (or expected/correct input first)\n" ++
  "   -t=TYPE                    sets the decode and format type\n" ++
  "                                e.g. i32, u8, f32, double\n" ++
  "                                use -h=t to list all types and symbols\n" ++
  "   -q/-v/-v2/-v3/-v=INT       sets the verbosity\n" ++
  "\n" ++
  " EXITS: given --diff exits non-zero if any files differ; otherwise zero\n" ++
  "        always zero in non --diff mode\n" ++
  ""

data Opts =
  Opts {
    oColor :: !Bool
  , oCols :: !Int
  , oDiff :: !Bool
  , oInputs :: ![FilePath]
  , oType :: !Type
  , oVerbosity :: !Int
  } deriving Show

dft_opts :: Opts
dft_opts =
  Opts {
    oColor = False
  , oCols = 0
  , oDiff = False
  , oInputs = []
  , oType = TypeU8
  , oVerbosity = 0
  }

data Type =
    TypeI8
  | TypeU8
  | TypeI16
  | TypeU16
  | TypeI32
  | TypeU32
  | TypeI64
  | TypeU64
  --
  | TypeF16
  | TypeF32
  | TypeF64
  | TypeBF16
  deriving (Show,Eq,Ord)


parseOpts :: Opts -> [String] -> IO Opts
parseOpts os []
  | null (oInputs os) = parseOpts os ["-h"]
  | otherwise = return os
parseOpts os (a:as)
  | a `elem` ["-h","--help"] = putStr usage >> exitSuccess
  | a`elem`["-h=t","-h=type"] = do
    putStr $
      concatMap (\(t,desc,_,_,nms) -> padR 48 (desc ++ ": ") ++ intercalate ", " nms ++ "\n") type_table
    exitSuccess
  | k`elem`["--color="] && v`elem`["auto"] = parseOpts os as
  | k`elem`["--color="] && v`elem`["on","true","1"] = parseOpts os {oColor = True} as
  | k`elem`["--color="] && v`elem`["off","false","0"] = parseOpts os {oColor = False} as
  | k`elem`["-c=","--cols"] = parseAsIntValue (\i -> os {oCols = i})
  | a`elem`["-d","--diff"] = parseOpts (os {oDiff = True}) as
  | k`elem`["-t=","--type="] =
    case find (\(_,_,_,_,nms) -> v`elem`nms) type_table of
      Just (t,_,_,_,_) -> parseOpts os {oType = t} as
      _ -> badArg "invalid type (use -h=t to list types)"
  | "-"`isPrefixOf`k = badArg "invalid option"
  | otherwise = parseOpts os{oInputs = oInputs os ++ [a]} as
  where (k,v) =
          case span (/='=') a of
            (k,'=':sfx) -> (k ++ "=",sfx)
            (k,"") -> (k,"")

        v_comma_sep :: [String]
        v_comma_sep = words (map (\c -> if c`elem`",+" then ' ' else c) v)

        parseAsValue func
          | null v = badArg "expects argument"
          | otherwise = parseOpts func as
        parseAsIntValue func =
          case reads v of
            [(x,"")] -> parseOpts (func x) as
            _ -> badArg "malformed integer"
        badArg msg = die (a ++ ": " ++ msg)

runD :: [String] -> IO ()
runD as = do
  is_tty <- hIsTerminalDevice stdout
  parseOpts dft_opts {oColor = is_tty} as >>= runWithOpts
runWithOpts :: Opts -> IO ()
runWithOpts os
  | oDiff os = do
    when (length (oInputs os) < 2) $ do
      fatal (oColor os) "--diff needs at least two files"
    bss <- mapM readF (oInputs os)
    case bss of
      ref:diffs -> do
        diff <- runWithOptsOnInput os diffs ref
        when diff exitFailure
  | null (oInputs os) = do
    bs <- BS.hGetContents stdin
    runWithOptsOnInput os [] ("<stdin>",bs)
    return ()
  | otherwise = mapM_ runF (oInputs os)
  where runF :: FilePath -> IO ()
        runF fp = readF fp >>= runWithOptsOnInput os  [] >> return ()
        readF :: FilePath -> IO (FilePath,BS.ByteString)
        readF fp = do {bs <- BS.readFile fp; return (fp,bs)}

fatal :: Bool -> String -> IO a
fatal color msg = do
  putStrFatal (msg ++ "\n")
  exitFailure

runWithOptsOnInput ::
  Opts ->
  [(FilePath,BS.ByteString)] ->
  (FilePath,BS.ByteString) -> IO Bool
runWithOptsOnInput os diff_bs0 (fp,bs0) = do
    putStr (fp ++ ":")
    if null diff_bs0 then putStrLn "" else do
      putStrLn " diffed against:"
      mapM_ (\fp -> putStrLn $ "  " ++ fp ++ ":") (map fst diff_bs0)
    emitRows True 0 bs0 (map snd diff_bs0)

  where cols_per_row :: Int
        cols_per_row =
          max 1 (if oCols os == 0 then 16`div`t_bytes_per_elem else oCols os)

        t_bytes_per_elem :: Int
        t_bytes_per_elem = tBytes (oType os)

        t_bytes_per_row :: Int64
        t_bytes_per_row = fromIntegral (cols_per_row * t_bytes_per_elem)

        big_file :: Bool -- >16K render 8 hex chars
        big_file = BS.length (BS.take (16*1024) bs0) > 16*1024

        fmtOff :: Int64 -> String
        fmtOff
          | big_file = printf "%08X:"
          | otherwise = printf "%05X:"

        -- for each row:
        --   split off a row's worth of bytes (also do so for the diff streams)
        --   emit the reference row
        --     complain row has leftovers (not multiple of element size)
        --   emit diff rows suppress empty rows (similarly compain about mismatch)
        --   compare reference row with diff rows
        emitRows :: Bool -> Int64 -> BS.ByteString -> [BS.ByteString] -> IO Bool
        emitRows equal _   bs_refs bs_diffs | all BS.null (bs_refs:bs_diffs) = return equal
        emitRows equal off bs_refs bs_diffs = do
          let indent = map (const ' ') (fmtOff off)
              (bs_ref,bs_ref_sfx) = splitRow bs_refs
              (bs_diffs_row,bs_diffs_sfxs) = unzip (map splitRow bs_diffs) -- [((elems,leftovers),suffix)]
          putStr $ fmtOff off
          emitRow True bs_ref Nothing
          forM_ bs_diffs_row $ \bs_diff@(bs_diff_row,bs_diff_row_leftover) -> do
            unless (bs_ref == bs_diff) $ do
              putStr $ map (const ' ') (fmtOff off)
              emitRow False bs_diff (Just bs_ref)
          let equal1 =
                   equal -- already different
                || any (bs_ref /=) bs_diffs_row
          emitRows equal1 (off + t_bytes_per_row) bs_ref_sfx bs_diffs_sfxs

        emitRow :: Bool -> ([BS.ByteString],BS.ByteString) -> (Maybe ([BS.ByteString],BS.ByteString)) -> IO ()
        emitRow ref (bs_elems,bs_leftovers) m_bs_ref = do
            case m_bs_ref of
              Just (bs_refs,bs_ref_leftovers) -- suppress diff lines that match
                | not ref && bs_elems == bs_refs && bs_leftovers == bs_ref_leftovers -> return ()
              _ -> do
                emitElems bs_elems (fmap fst m_bs_ref)
                when (not (BS.null bs_leftovers)) $
                  putStrYellow " (last element truncated)"
            putStrLn ""
          where emitElems :: [BS.ByteString] -> Maybe [BS.ByteString] -> IO ()
                emitElems [] _ = return ()
                emitElems (bs_elem:bs_elems) m_bs_refs = do
                  putStr " "
                  let str = tFormat (oType os) bs_elem
                  case m_bs_refs of
                    Nothing -> putStr str -- nothing to diff against
                    Just [] -> putStrRed str -- reference stream is truncated
                    Just (bs_ref:bs_refs) -- both stream have an element
                      | bs_ref /= bs_elem -> putStrRed str -- mismatches
                      | otherwise -> putStrGreen str -- matches
                  emitElems bs_elems (fmap (drop 1) m_bs_refs)

        splitRow :: BS.ByteString -> (([BS.ByteString],BS.ByteString),BS.ByteString)
        splitRow bs =
          case BS.splitAt t_bytes_per_row bs of
            (bs_row,bs_sfx)
              | m == 0 -> ((bsChunk t_bytes_per_elem bs_row,BS.empty),bs_sfx)
              | otherwise ->
                case BS.splitAt (BS.length bs_row - m) bs_row of
                  (bs_row,bs_leftover) -> ((bsChunk t_bytes_per_elem bs_row,bs_leftover),bs_sfx)
              where m = BS.length bs_row `mod` fromIntegral t_bytes_per_elem


type ElemBits = BS.ByteString

fmtChr :: Word8 -> Char
fmtChr w8
    | i >= 128 = '.' -- putStr will blow up for some chars (historically)
    | not (isPrint c) = '.'
    | otherwise = c
    where i = fromIntegral w8
          c = chr i

chunk :: Int -> [a] -> [[a]]
chunk k [] = []
chunk k as =
  case splitAt k as of
    (as_pfx,sfx) -> as_pfx:chunk k sfx


type TypeInfo = (Type,String,Int,BS.ByteString -> String,[String])
type_table :: [TypeInfo]
type_table =
    [
      ty  TypeI8 "8b signed integer" 1
        ["i8","s8","int8_t","char"]
        (fmtI8 . fromIntegral . fromByteStringU8)
    , ty  TypeU8 "8b unsigned integer" 1
        ["u8","uint8_t","uchar"]
        (fmtU8 .                fromByteStringU8)
    , ty TypeI16 "16b signed integer" 2
        ["i16","s16","short","int16_t"]
        (fmtI16 . fromIntegral . fromByteStringU16LE)
    , ty TypeU16 "16b unsigned integer" 2
        ["u16","ushort","uint16_t"]
        (fmtU16 .                fromByteStringU16LE)
    , ty TypeI32 "32b signed integer" 4
        ["i32","s32","int","int32_t"]
        (fmtI32 . fromIntegral . fromByteStringU32LE)
    , ty TypeU32 "32b unsigned integer" 4
        ["u32","uint","uint32_t"]
        (fmtU32 .                fromByteStringU32LE)
    , ty TypeI64 "64b signed integer" 8
        ["i64","s64","long","int64_t"]
        (fmtI64 . fromIntegral . fromByteStringU64LE)
    , ty TypeU64 "64b unsigned integer" 8
        ["u64","ulong","uint64_t"]
        (fmtU64 .                fromByteStringU64LE)
    --
    , ty TypeF16 "16b half-precision floating point" 2
        ["f16","half"]
        (fmtF16 .                 fromByteStringU16LE)
    , ty TypeBF16 "16b \"brain\"-floating point" 2
        ["bf16","bfloat","bfloat16"]
        (fmtBF16 .                fromByteStringU16LE)
    , ty TypeF32 "32b single-precision floating point" 4
        ["f32","float"]
        (fmtF32 . floatFromBits . fromByteStringU32LE)
    , ty TypeF64 "64b double-precision floating point" 8
        ["f64","double"]
        (fmtF64 . doubleFromBits . fromByteStringU64LE)
    ]
  where ty t desc sz syms fmt = (t,desc,sz,fmt,syms)
tLookup :: Type -> TypeInfo
tLookup t1 =
  case find (\(t,_,_,_,_) -> t == t1) type_table of
    Just ti -> ti
    Nothing -> error "INTERNAL ERROR: tLookup: invalid type"

bsChunk :: Int -> BS.ByteString -> [BS.ByteString]
bsChunk _ bs | BS.null bs = []
bsChunk t_sz bs =
  case BS.splitAt (fromIntegral t_sz) bs of
    (bs_elem,bs_sfx) -> bs_elem:bsChunk t_sz bs_sfx

tBytes :: Type -> Int
tBytes t = case tLookup t of {(_,_,tsz,_,_) -> tsz}
tFormat :: Type -> BS.ByteString -> String
tFormat t bs = case tLookup t of {(_,_,_,fmt,_) -> fmt bs}

