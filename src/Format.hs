module Format where

import Floats

import Data.Bits
import Data.Int
import Data.Word
import Text.Printf
import System.IO
import qualified System.Console.ANSI as SCA -- from ansi-terminal


fmtU8 :: Word8 -> String
fmtU8 = printf "%02X"
fmtU16 :: Word16 -> String
fmtU16 = printf "%04X"
fmtU32 :: Word32 -> String
fmtU32 = printf "%08X"
fmtU64 :: Word64 -> String
fmtU64 = printf "%016X"
fmtI8 :: Int8 -> String
fmtI8 = padR 4 . show
fmtI16 :: Int16 -> String
fmtI16 = padR 6 . show
fmtI32 :: Int32 -> String
fmtI32 = padR 12 . show
fmtI64 :: Int64 -> String
fmtI64 = padR 21 . show
--
fmtBF16 :: Word16 -> String
fmtBF16 w16 = printf "%12f" (floatFromBits w32)
  where w32 = (fromIntegral w16 :: Word32)`shiftL`16
fmtF16 :: Word16 -> String
fmtF16 = fmtF32 . floatFromBits . halfBitsToFloatBits
fmtF32 :: Float -> String
fmtF32 = fmt -- printf "%12f"
  where fmt x
          | length dec < 14 = dec
          | otherwise = printf "%14e" x
          where dec = printf "%14f" x
fmtF64 :: Double -> String
fmtF64 = fmt
  where fmt x
          | length dec < 18 = dec
          | otherwise = printf "%18e" x
          where dec = printf "%18f" x
--
padR :: Int -> String -> String
padR k s = s ++ replicate (k - length s) ' '

--------------------------------------------------------------------------------
putStrFatal :: String -> IO ()
putStrFatal = hPutColored stderr SCA.Vivid SCA.Red

putStrRed, putStrLnRed :: String -> IO ()
putStrRed = hPutColored stdout SCA.Vivid SCA.Red
putStrLnRed = hPutColoredLn stdout SCA.Vivid SCA.Red

putStrGreen, putStrLnGreen :: String -> IO ()
putStrGreen = hPutColored stdout SCA.Vivid SCA.Green
putStrLnGreen = hPutColoredLn stdout SCA.Vivid SCA.Green

putStrYellow, putStrLnYellow :: String -> IO ()
putStrYellow = hPutColored stdout SCA.Vivid SCA.Yellow
putStrLnYellow = hPutColoredLn stdout SCA.Vivid SCA.Yellow


hPutColored, hPutColoredLn :: Handle -> SCA.ColorIntensity -> SCA.Color -> String -> IO ()
hPutColored h i c s = do
  SCA.hSetSGR h [SCA.SetColor SCA.Foreground i c]
  hPutStr h s
  SCA.hSetSGR h [SCA.Reset]
hPutColoredLn h i c s = do
  SCA.hSetSGR h [SCA.SetColor SCA.Foreground i c]
  hPutStrLn h s
  SCA.hSetSGR h [SCA.Reset]
