{-# OPTIONS -fno-cse #-}
module Floats where

import Data.Bits
import Data.Int
import Data.Word
import Unsafe.Coerce(unsafeCoerce)

doubleToBits :: Double -> Word64
doubleToBits = unsafeCoerce
doubleFromBits :: Word64 -> Double
doubleFromBits = unsafeCoerce

floatToBits :: Float -> Word32
floatToBits = unsafeCoerce
floatFromBits :: Word32 -> Float
floatFromBits = unsafeCoerce

halfBitsToFloatBits :: Word16 -> Word32
halfBitsToFloatBits w16 = f_bits
  where f_bits
          -- | trace (printf "s: 0x%x, e: 0x%x (0x%x), m: 0x%x" f32_s f32_exp (setF32 f32_exp) f32_mnt) False = undefined
          | f16_exp == f16_exp_mask && f16_mnt /= 0 = f32_sgn .|. f32_EXP_MASK .|. fromIntegral f32_mnt -- nan
          | f16_exp == f16_exp_mask = f32_sgn .|. f32_EXP_MASK
          | f16_exp == 0 && f16_mnt == 0 = f32_sgn -- +/- 0.0
          | f16_exp == 0 = normalize (127 - 15 + 1) f16_mnt
             -- trace (printf "%x %x" f32_exp f32_mnt) $ norm (127 - 15 + 1) f16_mnt
            -- f32_sgn .|. setF32 (f32_exp + 1) .|. (f32_mnt `shiftR` 1) -- den. 16 can be norm. 32

          | otherwise = f32_sgn .|. setF32 f32_exp .|. f32_mnt -- normal case
          where setF32 :: Int32 -> Word32
                setF32 f = (0x1FF .&. fromIntegral f32_exp) `shiftL` 23

                normalize :: Word32 -> Word16 -> Word32
                normalize e32 m16
                  | (m16 .&. min_exp) == 0 = normalize (e32 - 1) (2*m16)
                  | otherwise = f32_sgn .|. (e32`shiftL`23) .|. m16_shifted
                  where min_exp = fromIntegral f16_mnt_mask + 1
                        m16_shifted = fromIntegral m16`shiftL`(23 - 10) -- .&. f32_MNT_MASK

        f16_sgn = f16_sign_bit .&. w16 :: Word16
        f16_exp = f16_exp_mask .&. w16 :: Word16
        f16_mnt = f16_mnt_mask .&. w16 :: Word16

        f32_sgn = fromIntegral f16_sgn `shiftL` 16 :: Word32
        f32_exp = f16_exp_val + 127 :: Int32
          where f16_exp_val = (fromIntegral f16_exp`shiftR`10) - 15
        f32_mnt = fromIntegral f16_mnt `shiftL` (23 - 10) :: Word32

f32_MNT_BITS :: Int
f32_MNT_BITS = 23
f32_EXP_BITS :: Int
f32_EXP_BITS = 8
f32_SIGN_BIT :: Word32
f32_SIGN_BIT = 1`shiftL`(f32_MNT_BITS + f32_EXP_BITS) -- 0x80000000
f32_EXP_MASK :: Word32
f32_EXP_MASK = ((1`shiftL`f32_EXP_BITS) - 1)`shiftL`f32_MNT_BITS -- 0x7F800000
f32_QNAN_BIT :: Word32
f32_QNAN_BIT = 1`shiftL`(f32_MNT_BITS - 1) -- 0x00400000
f32_MNT_MASK :: Word32
f32_MNT_MASK = (f32_QNAN_BIT`shiftL`1) - 1

f16_MNT_BITS :: Int
f16_MNT_BITS = 10
f16_EXP_BITS :: Int
f16_EXP_BITS = 5
f16_SIGN_BIT :: Word16
f16_SIGN_BIT = 1`shiftL`(f16_MNT_BITS + f16_EXP_BITS) -- 0x8000
f16_EXP_MASK :: Word16
f16_EXP_MASK = ((1`shiftL`f16_EXP_BITS) - 1)`shiftL`f16_MNT_BITS -- 0x7C00
f16_QNAN_BIT :: Word16
f16_QNAN_BIT = 1`shiftL`(f16_MNT_BITS - 1) -- 0x0200
f16_MNT_MASK :: Word16
f16_MNT_MASK = (f16_QNAN_BIT`shiftL`1) - 1 -- 0x03FF

f16_sign_bit :: Word16
f16_sign_bit = 0x8000
f16_exp_mask :: Word16
f16_exp_mask = 0x7C00
f16_qnan_bit :: Word16
f16_qnan_bit = 0x0200
f16_mnt_mask :: Word16
f16_mnt_mask = (f16_qnan_bit`shiftL`1) - 1
