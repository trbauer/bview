module Main where

import BView

import System.Environment

main :: IO ()
main = getArgs >>= run

run :: [String] -> IO ()
run = runD
