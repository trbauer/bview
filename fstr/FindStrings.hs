module Main where


import Control.Monad
import Data.Char
import Data.Bits
import Data.List
import Data.Int
import Debug.Trace
import System.IO
import System.Environment
import System.Exit
import Text.Printf
import qualified Data.ByteString.Lazy as LBS


main :: IO ()
main = getArgs >>= run
run :: [String] -> IO ()
run as = parseOpts dft_opts as >>= runWithOpts
runWithOpts :: Opts -> IO ()
runWithOpts os =
  forM_ (oInputs os) $ \inp -> do
    dumpChunks os inp

-- TODO:
--   1. parseOpts (verbose to dump OTHER)
--     -slen=INT for min string length
--     -seek=OFF for offset start
--
--   2. test it (is it fast enough?)
data Opts =
  Opts {
    oInputs :: ![FilePath]
  , oMinStringLength :: !Int
  , oStartOffset :: !Int
  , oMaxSearchLength :: !Int
  , oVerbosity :: !Int
  } deriving (Show,Eq)


dft_opts :: Opts
dft_opts =
  Opts {
    oInputs = []
  , oMaxSearchLength = maxBound
  , oMinStringLength = 8
  , oStartOffset = 0
  , oVerbosity = 0
  }

parseOpts :: Opts -> [String] -> IO Opts
parseOpts os [] = do
  when (null (oInputs os)) $
    die "input expected"
  return os
parseOpts os (a:as)
  | a`elem`["-h","--help"] = do
    putStrLn $
      "USAGE: fs [OPTS] PATHS+\n" ++
      "where OPTS are\n" ++
      "  -q/-v/-v2           verbosity level\n" ++
      "  -msl=INT            sets the minimum string length (defaults to " ++ show (oMinStringLength dft_opts) ++ ")\n" ++
      "  --seek=INT          seek starts the search from a specific offset\n" ++
      "  --length=INT        the maximum number of bytes to search (from -msl=...)\n" ++
      ""
    exitSuccess
  --
  | a == "-q" = nextArg os{oVerbosity = -1}
  | a == "-v" = nextArg os{oVerbosity = 1}
  | a`elem`["-v2"] = nextArg os{oVerbosity = 2}
  | k == "-v=" = parseAsIntValue (\i -> os{oVerbosity = i})
  --
  | k == "--length=" = parseAsIntValue (\i -> os{oMaxSearchLength = i})
  | k == "-msl=" = parseAsIntValue (\i -> os{oMinStringLength = i})
  | k == "--seek=" = parseAsIntValue (\i -> os{oStartOffset = i})
  --
  | "-"`isPrefixOf`a = badArg "invalid option"
  --
  | otherwise = nextArg os {oInputs = oInputs os ++ [a]}
  where (k,v) =
          case span (/='=') a of
            (k,'=':sfx) -> (k ++ "=",sfx)
            (inp,"") -> (inp,"")

        nextArg :: Opts -> IO Opts
        nextArg os = parseOpts os as

        parseAsIntValue :: (Int -> Opts) -> IO Opts
        parseAsIntValue func =
          case reads v of
            [(x,"")] -> parseOpts (func x) as
            _ -> badArg "malformed integer"
        badArg msg = die (a ++ ": " ++ msg)

help :: IO ()
help = do
  putStrLn "dumpChunks PATH"
  putStrLn "dumpChunks nvvm64"
  putStrLn ("dumpChunks " ++ show nvvm64)

nvvm64 :: FilePath
nvvm64 = "builds\\vs2022-64\\Debug\\nvvm64.dll"

dumpNvvm :: Int -> IO ()
dumpNvvm min_len = do
  dumpChunks (dft_opts {oMinStringLength = min_len}) nvvm64
  return ()

dumpChunks :: Opts -> FilePath -> IO ()
dumpChunks os fp = do
    bin_seek <- LBS.drop (fromIntegral (oStartOffset os)) <$> LBS.readFile fp
    let bin
          | oMaxSearchLength os == maxBound = bin_seek
          | otherwise = LBS.take (fromIntegral (oMaxSearchLength os)) bin_seek
    let cs = parseChunks (oMinStringLength os) bin

        fmt_off :: Int64 -> String
        fmt_off off = printf "%08X: " off

        fmt_chunk :: Chunk -> String
        fmt_chunk c =
          case c of
            ChunkSTRING8 off s -> fmt_off off ++ "CHAR8* => " ++ show s ++ "\n"
            ChunkSTRING16 off s -> fmt_off off ++ "CHAR16* => L" ++ show s ++ "\n"
            ChunkOTHER off lbs
              | oVerbosity os >= 1 -> fmt_off off ++ "OTHER: " ++ show (LBS.length lbs) ++ " B: " ++ fmt_lbs (LBS.take 16 lbs) ++ ": " ++ show (LBS.take 16 lbs) ++ "\n"
              | otherwise -> ""
            ChunkERROR off lbs err
              | oVerbosity os >= 1 -> fmt_off off ++ "ERROR: " ++ err ++ "\n"
              | otherwise -> ""
    putStr $ concatMap fmt_chunk cs


fmt_lbs :: LBS.ByteString -> String
fmt_lbs = concatMap (printf " %02X") . LBS.unpack

data Chunk =
    ChunkSTRING8 !Int64 !String
  | ChunkSTRING16 !Int64 !String
  | ChunkOTHER !Int64 !LBS.ByteString
  | ChunkERROR !Int64 !LBS.ByteString !String
  deriving (Show,Eq)

parseChunks :: Int -> LBS.ByteString -> [Chunk]
parseChunks min_string_len =
--    merge_others .
      loop 0
  where loop :: Int64 -> LBS.ByteString -> [Chunk]
        loop off lbs = loopTryString16 off lbs

        loopTryString16 :: Int64 -> LBS.ByteString -> [Chunk]
        loopTryString16 off lbs = loop16 "" off lbs
          -- take as many pairs of two until we find an ending 00`00
          where loop16 :: String -> Int64 -> LBS.ByteString -> [Chunk]
                loop16 rstr off_w16 lbs_w16 =
                    case rstr of
                      ""
                        -- minimum requires a 2B w_char and a 2B NUL char => 4
                        | LBS.length (LBS.take 4 lbs_w16) < 2 ->
                          debug16 ("underflowed init wchar*: " ++ show lbs_w16) $
                            loopTryString8 off lbs
                        | not (isReallyPrintable c) -> debug16 ("init is not printable char16")
                          loopTryString8 off lbs
                      _
                        | LBS.length lbs_w16_pfx < 2 -> debug16 ("underflows middle string") $
                          ChunkSTRING16 off (reverse rstr) :
                            ChunkERROR off_w16 LBS.empty "EOF in wchar_t string" :
                              loopTryOtherWithPfx LBS.empty off_w16 lbs_w16
                        | isReallyPrintable c -> debug16 (show off ++ ": appending char16 " ++ show c) $
                          loop16 (c:rstr) (off_w16 + 2) lbs_w16_sfx
                        | int_chr == 0 -> debug16 ("found \\NUL (at_min_len=" ++ show at_min_len++")") $
                          -- ignore short valid strings
                          if not at_min_len
                            then loopTryString8 off lbs
                            else ChunkSTRING16 off_w16 (reverse rstr) : loop (off_w16 + 2) lbs_w16_sfx
                        | otherwise -> debug16 ("hit junk char") $
                          -- ignore short junk
                          if not at_min_len
                            then loopTryString8 off lbs
                            else
                              ChunkSTRING16 off (reverse rstr) :
                                ChunkERROR off_w16 lbs_w16_pfx "unexpected wchar_t in string" :
                                  loop (off_w16 + 2) lbs_w16_sfx

                  where (lbs_w16_pfx,lbs_w16_sfx) = LBS.splitAt 2 lbs_w16
                        [w16_lo,w16_hi] = LBS.unpack lbs_w16_pfx
                        int_chr :: Int
                        int_chr = ((fromIntegral w16_hi :: Int)`shiftL`8) .|. (fromIntegral w16_lo)
                        c :: Char
                        c = chr int_chr

                        at_min_len = min_string_len <= 0 || length rstr > min_string_len

                        debug16 msg = debug (show off ++ ": loopTryString16.loop16 " ++ show off_w16 ++ ": " ++ msg)

        loopTryString8 :: Int64 -> LBS.ByteString -> [Chunk]
        loopTryString8 off lbs = loop8 "" off lbs
          where loop8 :: String -> Int64 -> LBS.ByteString -> [Chunk]
                loop8 rstr off_w8 lbs_w8 =
                    case rstr of
                      ""
                        | LBS.null lbs_w8 -> loopTryOtherWithPfx LBS.empty off lbs
                        | not (isReallyPrintable c) ->
                          debug8 ("first char not printable; backing up to " ++ show off ++ " to start OTHER") $
                            loopTryOtherWithPfx LBS.empty off lbs
                      _
                        | LBS.null lbs_w8 -> debug8 ("at EOS; ABEND of char string") $
                          ChunkSTRING8 off (reverse rstr) :
                            ChunkERROR off_w8 LBS.empty "EOF in char string" :
                              loop (off_w8 + 1) lbs_w8_sfx
                        | isReallyPrintable c ->
                          debug8 ("appending c8 " ++ show c ++ " 0x" ++ printf "%02X" w8) $
                            loop8 (c:rstr) (off_w8 + 1) lbs_w8_sfx
                        | w8 == 0 ->
                          debug8 ("found \\NUL (at_min_len=" ++ show at_min_len++")") $
                            if not at_min_len
                              -- YES: off_w8 (otherwise tryChar will pass)
                              then loopTryOtherWithPfx (LBS.take (off_w8 - off) lbs) off_w8 lbs_w8
                              else ChunkSTRING8 off (reverse rstr):loop (off_w8 + 1) lbs_w8_sfx
                        | otherwise ->
                          if not at_min_len
                            then loopTryOtherWithPfx (LBS.take (off_w8 - off) lbs) off_w8 lbs_w8
                            else
                              ChunkSTRING8 off (reverse rstr) :
                                ChunkERROR off_w8 (LBS.pack [w8]) "unexpected char in string" :
                                  loop (off_w8 + 1) lbs_w8_sfx
                  where Just (w8,lbs_w8_sfx) = LBS.uncons lbs_w8
                        c = chr (fromIntegral w8)
                        debug8 msg = debug (show off ++ ": loopTryString8.loop8 " ++ show off_w8 ++ ": " ++ msg)

                        at_min_len = min_string_len <= 0 || length rstr > min_string_len


        loopTryOtherWithPfx :: LBS.ByteString -> Int64 -> LBS.ByteString -> [Chunk]
        loopTryOtherWithPfx pfx off lbs
          | LBS.null lbs = []
          | otherwise = loop_o off lbs
          where loop_o :: Int64 -> LBS.ByteString -> [Chunk]
                loop_o off_o lbs_o
                  | LBS.null lbs_o = debug_o ("finished OTHER at EOS " ++ show lbs_oth) $
                    [ChunkOTHER lbs_oth_off lbs_oth]
                  | otherwise =
                    case (tryCharPrintable lbs_o, tryWcharPrintable lbs_o) of
                      (Just (c8,_),_)
                        | off_o == off -> error $ "loopTryOther: unreachable " ++ show c8
                        | otherwise -> finish_chunk ("c8=" ++ show c8)
                      (_,Just (c16,_))
                        | off_o == off -> error $ "loopTryOther: unreachable " ++ show c16
                        | otherwise -> finish_chunk ("c16=" ++ show c16)
                      _ -> debug_o ("adding byte") $
                        loop_o (off_o + 1) (LBS.drop 1 lbs_o)
                  where finish_chunk s = debug_o ("finished OTHER; looking at " ++ s) $
                          ChunkOTHER lbs_oth_off lbs_oth : loop off_o lbs_o
                        lbs_oth = pfx`LBS.append`LBS.take (off_o - off) lbs
                        lbs_oth_off = off - LBS.length pfx

                        debug_o msg = debug (show lbs_oth_off ++ ": loopTryOtherWithPfx.loop_o " ++ show off_o ++ ": " ++ msg)

        debug :: String -> a -> a
        debug msg
          -- | True = trace msg
          | otherwise = id

        merge_others :: [Chunk] -> [Chunk]
        merge_others cs =
            case cs of
              ChunkOTHER off0 bs0:ChunkOTHER off1 bs1:cs ->
                merge_others (ChunkOTHER off0 (bs0`LBS.append`bs1):cs)
              c:cs -> c : merge_others cs
              [] -> []

tryCharPrintable :: LBS.ByteString -> Maybe (Char,LBS.ByteString)
tryCharPrintable = filterPrintable tryChar
tryWcharPrintable :: LBS.ByteString -> Maybe (Char,LBS.ByteString)
tryWcharPrintable = filterPrintable tryWchar

filterPrintable :: (LBS.ByteString -> Maybe (Char,LBS.ByteString)) -> LBS.ByteString -> Maybe (Char,LBS.ByteString)
filterPrintable fun lbs =
  case fun lbs of
    Nothing -> Nothing
    r@(Just (c,sfx))
      | isReallyPrintable c -> r
      | otherwise -> Nothing

tryChar :: LBS.ByteString -> Maybe (Char,LBS.ByteString)
tryChar lbs
  | LBS.null lbs = Nothing
  | otherwise = Just (chr (fromIntegral w8),lbs_w8_sfx)
  where Just (w8,lbs_w8_sfx) = LBS.uncons lbs

tryWchar :: LBS.ByteString -> Maybe (Char,LBS.ByteString)
tryWchar lbs
  | LBS.length lbs_w16_pfx < 2 = Nothing
  | otherwise = Just (chr int_chr,lbs_w16_sfx)
  where (lbs_w16_pfx,lbs_w16_sfx) = LBS.splitAt 2 lbs
        [w16_lo,w16_hi] = LBS.unpack lbs_w16_pfx
        int_chr :: Int
        int_chr = ((fromIntegral w16_hi :: Int)`shiftL`8) .|. (fromIntegral w16_lo)
        c :: Char
        c = chr int_chr

{-
        tryCharStr :: LBS.ByteString -> Maybe (Char,LBS.ByteString)
        tryCharStr lbs = do
          (c0,lsb_sfx) <- tryChar
          if not (isReallyPrintable c0) then Nothing
          else



help :: IO ()
help = do
  putStrLn $ "dumpChunk " ++ show nvvm64 ++ " SOFF"
  putStrLn $ "dumpChunk " ++ show nvvm64 ++ " 0x00"
-- DIAGNOSTICS
-- dumpChunk "builds\\vs2022-64\\Debug\\nvvm64.dll" 0x1A937C0 0x700
-- dumpChunk "builds\\vs2022-64\\Debug\\nvvm64.dll" 0x1CA74E0 0x700
-- dumpChunk "builds\\vs2022-64\\Debug\\nvvm64.dll" 0x1CA74E0 0x700

dumpChunk :: LBS.ByteString -> Int64 -> Int64 -> IO (LBS.ByteString,Int64)
dumpChunk lbs soff len  = do
    findNextString 0 (LBS.drop (fromIntegral soff) bin)
  where findNextString :: Int64 -> LBS.ByteString -> IO (LBS.ByteString,Int64)
        findNextString off lbs
          | (len > 0 && off >= fromIntegral len) || LBS.null lbs = return (lbs,off)
          | otherwise = do
            case LBS.span (==0) lbs of
              (lbs_zeros,lbs_sfx)
                | LBS.null lbs_zeros -> dumpString off lbs_sfx
                | otherwise -> do
                  putStrLn (show (LBS.length lbs_zeros) ++ " zeros")
                  dumpString (off + LBS.length lbs_zeros) lbs_sfx

        dumpString :: Int64 -> LBS.ByteString -> IO (LBS.ByteString,Int64)
        dumpString off lbs
          | (len > 0 && off >= fromIntegral len) || LBS.null lbs = return (lbs,off)
          | otherwise =
            case LBS.span (/=0) lbs of
              (lbs_nonzeros,lbs_sfx)
                | LBS.null lbs_nonzeros -> findNextString off lbs_sfx
                | otherwise -> do
                  -- debug which char blows us up
                  -- putStr $ printf "%05X+%04X: " (fromIntegral soff :: Int) off
                  -- mapM_ (putStr . printf " %02X") (LBS.unpack lbs_nonzeros) >> putStrLn ""
                  putStr $ printf "%05X: " (fromIntegral soff + off)
                  let txt = map (chr . fromIntegral) (LBS.unpack lbs_nonzeros)
                      (txt_good,txt_bad) = span isReallyPrintable txt
                  putStrLn txt_good
                  if null txt_bad
                    then findNextString (off + LBS.length lbs_nonzeros) lbs_sfx
                    else do
                      putStrLn " stopping due to non-printable chars"
                      return (lbs_sfx,LBS.length lbs_nonzeros + off)

-}

isReallyPrintable :: Char -> Bool
isReallyPrintable c = ord c < 0x80 && isPrint c

