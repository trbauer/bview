INSTALL_DST=c:/bin/

main:
	@echo make bv, fstr
	@echo make install

SRCS:=$(wildcard src/*.hs)
FSTR_SRCS:=$(wildcard fstr/*.hs)

bv: bv.exe
bv.exe: Makefile $(SRCS)
	mkdir -p build/bv
	ghc -j -hidir build/bv -odir build/bv --make src/BView.hs -O2 -o $@ -with-rtsopts="-N" -rtsopts -isrc -threaded
	strip $@
fstr: fstr.exe
fstr.exe: Makefile $(FSTR_SRCS)
	mkdir -p build/fs
	ghc -j -hidir build/bv -odir build/bv --make fstr/FindStrings.hs -O2 -o $@ -with-rtsopts="-N" -rtsopts -ifstr -threaded
	strip $@

install: bv.exe
	@cp bv.exe fstr.exe ${INSTALL_DST}


clean:
	rm -rf build *.exe

